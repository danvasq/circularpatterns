//
//  DvCircle.cpp
//  circularPatterns
//
//  Created by Daniel Vasquez on 2014-05-25.
//
//

#include "DvCircle.h"
#include "cinder/app/AppBasic.h"



using namespace ci;
using namespace ci::app;



DvCircle::DvCircle(){}

DvCircle::DvCircle(Vec2f pos, float radius)
{
    this->position = pos;
    this->radius = radius;
    this->color = Color(01.0f, 1.0f, 1.0f);
    
    mCircleName = "(set name)";
    mDisplayName = false;
    mTextBasePos = this->position + Vec2f(5.0f, -5.0f);
    mFont = Font( "Quicksand Light", 14 );
    mTextureFont = gl::TextureFont::create( mFont );
}


void DvCircle::draw()
{
    gl::color( color);
    gl::drawStrokedCircle(position, radius);
    //gl::drawSolidCircle(position, mRadius);
    if (mDisplayName)
    {
        mTextureFont->drawString(mCircleName, mTextBasePos);
    }
    
}

void DvCircle::setName(std::string name)
{
    this->mCircleName = name;
}


void DvCircle::setColor(Color clr)
{
    this->color = clr;
}


void DvCircle::setPosition(Vec2f pos){
    position = pos;
}


bool DvCircle::isInside(Vec2i pos)
{
    return pow((pos.x - position.x), 2.0) + pow((pos.y - position.y), 2.0) < pow(radius, 2.0);
}

void DvCircle::displayName(bool display)
{
    this->mDisplayName = display;
}
