//
//  DvCircle.h
//  circularPatterns
//
//  Created by Daniel Vasquez on 2014-05-25.
//
//

#pragma once
#include "cinder/Vector.h"
#include "cinder/Color.h"
#include "cinder/gl/TextureFont.h"
#include "cinder/gl/gl.h"

#include <string>
#include <vector>
#include <iostream>


class DvCircle {
    
public:
    DvCircle();
    DvCircle(ci::Vec2f, float);
    void draw();
    void setName( std::string );
    void setColor( ci::Color );
    void move( ci::Vec2f );
    void setPosition(ci::Vec2f);
    bool isInside(ci::Vec2i);
    void displayName(bool);
    
    int index;

    ci::Vec2f position;
    float radius;
    ci::Color color;
    
    // TEXT
    std::string             mCircleName;
    ci::Vec2f               mTextBasePos;
    ci::Rectf               mTextBoundsRect;
    ci::Font				mFont;
    ci::gl::TextureFontRef	mTextureFont;
    bool                    mDisplayName;
    
};
