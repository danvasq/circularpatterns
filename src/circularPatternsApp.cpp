#include "cinder/app/AppNative.h"
#include "cinder/gl/gl.h"

#include <vector>
#include <math.h>
#include "dvCircle.h"

#define PI 3.14159265

using namespace ci;
using namespace ci::app;
using namespace std;

#define WINDOW_WIDTH 720
#define WINDOW_HEIGHT 720

class circularPatternsApp : public AppNative {
  private:
    int concentricity;
    float incr;
    vector<DvCircle> circles;

  public:
    void prepareSettings(Settings*);
	void setup();
	void mouseDown( MouseEvent event );	
	void update();
	void draw();
    float overlap(DvCircle, DvCircle);
    Vec2f getParametricCoords(DvCircle, float);
    
    // Variables
    DvCircle cA;
    DvCircle cCircle1;
    DvCircle cB;
};


void circularPatternsApp::prepareSettings( Settings *settings )
{
	settings->setWindowSize( WINDOW_WIDTH, WINDOW_HEIGHT );
	settings->setFrameRate( 60.0f );
}
void circularPatternsApp::setup()
{
    concentricity = 3;
    incr = 0.0f;
    
    // Build Circle A
    cA = DvCircle(ci::Vec2f(WINDOW_WIDTH/2.0f,WINDOW_HEIGHT/2.0f), 150.0f);
    cA.setName("A");
    cA.displayName(true);
    
    // Build concentric circle 1
    cCircle1 = DvCircle(cA.position, cA.radius * 1.6f);
    cCircle1.setColor( Color(0.8f, 0.2f, 0.3f) );
    
    // Build circle B
    float cB_radius = cCircle1.radius - cA.radius;
    cB = DvCircle( getParametricCoords(cCircle1, 0), cB_radius);
    cB.setName("B");
    cB.displayName(true);


}

void circularPatternsApp::mouseDown( MouseEvent event )
{
}

void circularPatternsApp::update()
{
    incr = incr + 2.1f;
}

void circularPatternsApp::draw()
{
	// clear window
	gl::clear( Color( 0, 0, 0 ) );
    
    // Increasing degrees
//    float deg = fmod(incr, 360.0f);
    
//    for (int c = 0; c < concentricity; c++)
//    {
//        DvCircle acircle = DvCircle(ci::Vec2f(getWindowWidth()/2.0f,getWindowHeight()/2.0f), 30.0f);
//        acircle.draw();
//    }
    // Overlap test
//    DvCircle overCircle = DvCircle(ci::Vec2f(incr,incr), 30.0f+incr);
    cA.draw();
//    overCircle.draw();
//    float overlapMag = overlap(cA,overCircle);
//    if ( overlapMag < 0 )
//    {
//        cout << "overlap = " << overlapMag << endl;
//    }
//    cout << "deg = " << deg << endl;
    
    cCircle1.draw();

    gl::drawLine(cA.position, cB.position);
    cB.draw();
    
    // thoughts...
//    float angle1 = 45.0f;
//    float tanA_len = 200.0f;
//    Vec2f tanA_b = getParametricCoords(cA, angle1);
//    float distance = sqrt( pow(cA.radius, 2.0f) + pow(tanA_len, 2.0f) );
//    float angle2 = (float) atan(tanA_len/cA.radius);
    

}

float circularPatternsApp::overlap(DvCircle c1, DvCircle c2)
{
    Vec2f pos1 = c1.position;
    Vec2f pos2 = c2.position;
    float distance = sqrt( pow(pos1.x - pos2.x, 2.0f) + pow(pos1.y - pos2.y, 2.0f) );
    
    return (distance - c1.radius) - c2.radius - 1.0f;
}

Vec2f circularPatternsApp::getParametricCoords(DvCircle circle, float angle)
{
    float x = circle.position.x + circle.radius * cos( angle * PI / 180.0 );
    float y = circle.position.y + circle.radius * sin( angle * PI / 180.0 );
    
    return Vec2f(x,y);
}



CINDER_APP_NATIVE( circularPatternsApp, RendererGl )
